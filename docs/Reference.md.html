<meta charset="utf-8" emacsmode="-*- markdown -*-"><link rel="stylesheet" href="markdeep/apidoc.css?"">

                          **Animatron Reference**

This document exposes the API available (via OSC commands) through
Animatron.

# OSC Commands

!!! Note
    OSC argument types are specified by a suffix after the name: `s`
    is a **string**, `i` is an **integer**, `f` is a **float**, `b`
    is a **boolean** (can also be sent as a `0` or `1` integer).

## Actor commands

!!! Note
    Actor commands take an initial `actor` argument, and will apply to
    that target. The target actor argument can be omitted and a `!`
    appended to the command address to apply the command to the
    current set of selected actors. Alternatively, you may explicitly
    specify the current selection using a `"!"` argument for the actor
    name.
    
    When the `actor` argument is used, it will look for actor targets
    matching that name. Note that `*` (match zero or more characters)
    and `?` (match exactly one character) may be used as actor name
    wildcards.

Address | Args <img width=675px/> | Description
--------|------|------------
`/free` <br/> `/free!` | `actor:s` <br/> - | Delete the actor (remove its instance).
`/play` <br/> `/play!` | `actor:s` <br/> - | Play the actor's animation from the current frame.
`/stop` <br/> `/stop!` | `actor:s` <br/> - | Stop playing the actor's animation.
`/frame` <br/> `/frame!` | `actor:s frame:i` <br/> `frame:i` | Jump to the given frame number (with wrapping).
`/position` <br/> `/position!` | `actor:s x:f y:f [dur:f]` <br/> `x:f y:f [dur:f]` | Set the actor to the x,y coordinates (specified as fractions of the viewport from 0-1). If `dur` is set, interpolate the movement over that number of seconds, otherwise change instantaneously.
`/rotate` <br/> `/rotate!` | `actor:s rot:f [dur:f]` <br/> `rot:f [dur:f]` | Set the actor's rotation to `rot` degrees. If `dur` is set, interpolate the rotation over that number of seconds, otherwise change instantaneously.
`/scale` <br/> `/scale!` | `actor:s scl:f [dur:f]` <br/> `scl:f [dur:f]` | Set the actor's scale. If `dur` is set, interpolate the scale over that number of seconds, otherwise change instantaneously. `scl` can be a single `float` for uniform scaling, or it can be a comma-separated string of two numbers, to specify different x,y scaling. <br/> (e.g. `/scale myActor 2.5,1 4`).
`/pivot` <br/> `/pivot!` | `actor:s px:f py:f [dur:f]` <br/> `px:f py:f [dur:f]` | Set the actor's animation pivot to the specified point. `px` and `py` should be in the normalized range 0-1, with 0 being left/top of the frame and 1 being right/bottom. If `dur` is set, interpolate the pivot over that number of seconds, otherwise change instantaneously.
`/fade` <br/> `/fade!` | `actor:s opacity:f [dur:f]` <br/> `opacity:f [dur:f]` | Fade the actor (set its opacity, where 1 is fully opaque/visible and 0 is transparent/invisible). If `dur` is set, interpolate the fade over that number of seconds, otherwise change instantaneously.
`/speed` <br/> `/speed!` | `actor:s rate:f` <br/> `rate:f` | Set the playback speed (1 is normal speed).
`/loop` <br/> `/loop!` | `actor:s [enable:b]` <br/> `[enable:b]` | Enable or disable looped animation playback. `enable` argument defaults to `true`, and initial state of animation looping for new actors is also `true`.
`/fliph` <br/> `/fliph!` | `actor:s` <br/> - | Flip the actor horizontally.
`/flipv` <br/> `/flipv!` | `actor:s` <br/> - | Flip the actor vertically.
`/color` <br/> `/color!` | `actor:s r:f g:f b:f` <br/> `r:f g:f b:f` | Add an RGB colour to the actor. Red, green and blue should be in the 0-1 range (can be negative to subtract colour). Set to black (0,0,0) to restore its original colour.
`/say` <br/> `/say!` | `actor:s text:s [dur:f]` <br/> `text:s [dur:f]` | Have the actor "say" something via a speech bubble, with an optional duration (defaults to three seconds).
`/action` <br/> `/action!` | `actor:s [action:s] [args...]` <br/> `[action:s] [args...]` | Apply an action (behaviour) to the actor. Actions are script classes that define custom behaviours, such as "wander". Each action type will have its own unique list of arguments. You can see the [built-in actions here](#actions). Calling it without any `action` will remove any existing actions.
`/behind` <br/> `/behind!` | `actor:s ref:s` <br/> `ref:s` | Change the draw order for an actor to be behind a reference actor. To move to the background, use `/behind actor *`.
`/front` <br/> `/front!` | `actor:s ref:s` <br/> `ref:s` | Change the draw order for an actor to be in front of a reference actor. To move to the foreground, use `/front actor *`.

## Other commands

Address | Args <img width="1000px"/> | Description
--------|------|------------
`/load` | `asset:s` | Load an (animation) asset from disk. It will create an `anim` with the same name as the asset. Wildcards are supported, so several animations can be loaded at once. The list of assets available to load can be found by calling `/list/assets`.
`/create` | `actor:s anim:s` | Create a new actor with a given (loaded) animation. If the named actor already exists, change its animation to the one specified. The initial position of a new actor will be at (0.5,0.5), the centre of the viewport.
`/createordestroy` | `actor:s anim:s` | Like `/create`, but if the actor already exists, it removes it.
`/ysort` | `[sort:b]` | Force actors to be sorted based on y position (lower on screen drawn later, i.e. "on top"). `sort` argument defaults to `true`, but initial state of y-sorting at startup is `false`.
`/list` <br/> `/list/actors` | | Get list of current actor instances. Returned in the form of a `/list/actors/reply` OSC message.
`/list/anims` | | Get list of available (loaded) animations. Returned in the form of a `/list/anims/reply` OSC message.
`/list/assets` | | Get list of available (unloaded) assets on disk. Returned in the form of a `/list/assets/reply` OSC message.
`/group` | `group:s [actor:s]` | Add the actor to a named group. If run without `actor` it will return the list of actors in the group. Note that `/group "myGroup" "!"` may be used to add the current selection to a group.
`/ungroup` | `group:s actor:s` | Remove the actor from a named group.
`/select` | `[actor:s]` | Add the actor to the selected set. If run without `actor` it will return the list of selected actors.
`/deselect` | `[actor:s]` | Remove the actor from the selected set. If run without `actor` it will deselect all (equivalent to `/deselect "*"` or `/deselect "!"`).
`/selected` | | Get the list of selected actors.
`/def` | `cmdName [subCommand] ...` | Define a custom OSC command that is a list of other OSC commands. This may be recursive, so each `subCommand` may reference one of the built-in commands, or another custom-defined command. Another way to define custom commands is via the file `commands/init.osc`. The `cmdName` string (first argument) may include argument names, which may be referenced as `subCommand` arguments using `$name`. Example: `/def "/addsel actor anim" "/create $actor $anim" "/select $actor"`.
`/load/defs` | `filename` | Load a custom command definitions file, which should have the format [described below](#def-files).
`/debug` | `[enable:b]` | Enable (or disable) reporting of informative status messages (via OSC and to the debug window). By default, this is disabled when running in exported builds, but enabled when running in the Godot editor. This flag does not affect error reporting (errors are always reported).
`/wait` | `duration` | Wait for `duration` seconds before executing the next command. Only relevant within a list of commands being executed together at once, such as inside a custom `def`ined command list.
`/load/alphabet` |  | Load the alphabet (default letters load from folders named `letter-[ASCII_CHAR]`)
`/letter` | `letter:c asset:s` | Map a single letter to an asset.  Creates an actor named after LETTER with ASSET.
`/write`| `msg:s`| Write MSG string with assets mapped to letters.
`/letter/spacing` | `amount`| Set the spacing between letters.  It's a multiplier of the actors' size.
`/letter/scale` | `scale`| Set the scale of the letters.

## Configuration commands
Address | Args <img width=100px/> | Description
--------|------|------------
`/load/config` | `filename:s` | Load a config file following the `def` syntax (**WARNING** This might change in the futre).  After loading the file, the method defined with `def` needs to be explicitly called.
`/assets/path` | `[path:s]` | Sets the path to the assets root directory.  If no argument is provided, it returns the current value of the path.
`/app/remote` | `[allow:b]` | Allow (or prevent) executing OSC commands from remote (non-localhost) clients.
`/window/screen` | `screenIndex:i` | Move the window between displays (0 = main display).
`/window/position` | `x:i y:i` | Move window to the specified position given in pixels.
`/window/size` | `sx:i sy:i` | Set window to the specified size in pixels. -1 may be specified for either `sx` or `sy`, in which case it will be computed using the project viewport aspect ratio.
`/window/center` | | Center window in main display.
`/window/fullscreen` | `[enable:b]` | Set fullscreen mode (defaults to true).
`/window/top` | `[enable:b]` | Set window to be always on top of other windows (defaults to true).

# OSC Replies

These are the OSC messages that can be sent from Animatron to a client
application, such as SuperCollider, which is sending OSC commands.

Address | Args | Description
--------|------|------------
`/error/reply` | `error:s` | Return an error message to the client.
`/status/reply` | `status:s` | Return a status message to the client.
`/list/actors/reply` | `[actor:s] ...` | Return the list of instanced actors to the client (one name per argument).
`/list/anims/reply` | `[anim:s] ...` | Return the list of available (loaded) animations to the client (one name per argument).
`/list/assets/reply` | `[asset:s] ...` | Return the list of available (unloaded) assets to the client (one name per argument). Assets must be loaded as anims in order to create actor instances.

# Custom sprite sheets

You must place PNG or JPEG sprite sheet files in the `animations/`
directory under the Animatron folder. These are considered "assets",
and will be loaded as sprite sheets at launch and made available as a
library of animations.  There is a naming convention to specify the
sprite layout and frame rate:

`NAME[_DIRSdir]_COLSxROWS_FPSfps.png`

where:
- *NAME* - is the name of the animation, by which it will be identified in commands.
- *DIRS* - (optional) is the number of directions in the spritesheet. We currently only support `_8dir` (or `_1dir`, in which case this field can be omitted in the filename).
- *COLS* - number of columns of the spritesheet
- *ROWS* - number of rows of the spritesheet
- *FPS* - animation's framerate

!!! Tip
    The separators between *NAME* and *DIRS* or *COLS* and between *ROWS* and *FPS*
    **must be underlines** (`_`), not hyphens (`-`). Use hyphens if you want dividers
    in the animation name; underscores are reserved to delimit "metadata".

Here is an example:

`my-animation_6x4_12fps.png` would load as an animation called
`my-animation` using a 6x4 sprite layout, playing at 12 frames per
second. `my-animation_8dir_12x16_24fps.png` would load as an 8-way
(S, SE, E, NE, ...) set of animations, where the spritesheet
contains 12 columns and 16 rows (two rows per compass direction),
and would play at 24 frames per second. In either case, an actor
could then be created from it using an OSC message like this:

`/create, "aname", "my-animation"`
             
For files that don't follow this (*name_dims_rate.png*) naming
convention, the entire filename will be used as the animation name in
Animatron, and they will be assumed to contain 4x4 sprites and play at
24 fps.

# <a name="def-files" />OSC custom command files

You may define a set of custom OSC commands (similar to functions or
macros that perform a list of other commands) using the `/def` OSC
command itself, or by loading text files containing custom command
definitions. At startup, the file `commands/init.osc` is parsed with
an initial set of definitions. You may also load command definition
files at runtime using the `/load/defs` command (its argument should
be a file relative to the `commands/` directory).

These files should contain lines with `def /commandName` or `def
/commandName arg1 [arg2] ...`, followed by a list of OSC commands, one
per line, with the commands/addresses and their arguments separated by
spaces).  Quotes are not needed around arguments, unless the arguments
are strings that should contain spaces.

Custom OSC commands can refer to other custom commands recursively,
and no validation is done at load time, so they may be referenced
before they are defined.

Here is an example `init.osc` file:

```
def /startup
    /load om-walk-*
    /wait 2
    /create walker om-walk-s
    /color walker 0.7 0.2 0.1
    /wait 1
    /crowd
    /wait 3
    /say walker "Hi everybody!"

def /createsel actor anim
    /create $actor $anim
    /select $actor

def /crowd
    /deselect
    /createsel om1 om-walk-w
    /createsel om2 om-walk-w
    /createsel om3 om-walk-w
    /createsel om4 om-walk-w
    /createsel om5 om-walk-w
    /group crowd om?
```

<a name="actions"/>
# Actions (behaviours)

This the the list of built-in actions and their arguments, which can
be applied to actors using the `/action` OSC command, for example:
`/action myActor wander 0.25 2`.

Name | Args <img width=675px/> | Description
-----|-------------------------|------------
`wander` | `range:f speed:f` | Make the actor wander around its current position. The maximum `range` is as a fraction of the view height (defaults to `0.1`), and the `speed` is a multiplier on the base rate of the effect (defaults to `1.0`).
`oscillate` | `period:f rotRange:f posRangeX:f posRangeY:f posFreqX:f posFreqY:f posPhase:f` | Give the actor a periodic oscillating behaviour. The position forms a lissajous shape (with range, normalized frequency and phase) and a rotation range in degrees. The `period` is in seconds, and defines the time to complete one cycle. To get a "cartoon walking" behaviour, you can use: `/action axe oscillate 1 15 0.05 0.1 1 2 0.125`. For a circular motion, you could use: `/action myActor oscillate 1 0 0.1 0.1 1 1 0.25`.
`wrap` | `moveDir:f moveSpeed:f rotSpeed:f` | Make the actor move in a fixed direction at a given speed, wrapping the position around the viewport borders. It also allows continuous rotation (if desired). `moveDir` is in degrees; `moveSpeed` is in normalized (proportional to view size) units per second; `rotSpeed` is in degrees per second.


<style class="fallback">body{visibility:hidden}</style><script>markdeepOptions={tocStyle:'none'};</script>
<!-- Markdeep: --><script src="markdeep/markdeep.min.js?" charset="utf-8"></script>

